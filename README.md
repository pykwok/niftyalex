
# Pre-requisites

* VirtualBox
* pip install vagrant

# Procedure


Clone this repo:
```
git clone https://gitlab.mana.ericssondevops.com/EJOJMJN/niftyalex.git
cd niftyalex
```

## Your have no VM with Docker:
* Vagrant creates the VM
* Vagrant installs Docker on the VM
* Vagrant builds the Docker image on the VM
* Vagrant runs nifyalex as a container on the VM

```
git clone https://gitlab.mana.ericssondevops.com/EJOJMJN/niftyalex.git
cd niftyalex
vagrant up
```

## You have a VM with Docker:

Build the Docker image:

```
git clone https://gitlab.mana.ericssondevops.com/EJOJMJN/niftyalex.git
cd niftyalex
docker build -t niftyalex .
```
  
Start niftyalex as a container:

* map localhost:80 to container:80
* make local directory /alex available in container under /data/alex/libraries
  
```
  docker run -it -p 80:80 -v /alex:/data/alex/libraries niftyalex
```

# Extra stuff - ignore for now

QEMU for Windows - only need Tools + Libraries:
https://qemu.weilnetz.de/w64/

Add C:\Program Files\qemu to Windows PATH.

On the VM - create gigantic zero-filled file and delete it.
dd if=/dev/zero of=/zero ; rm -f /zero

Stop the VM

Convert to qcow2 and compress it while you're at it:

cd /cygdrive/c/Users/ejojmjn/VirtualBox VMs/niftyalex_default_1551820395093_99430
qemu-img convert ubuntu-xenial-16.04-cloudimg.vmdk -O qcow2 -c niftyalex.qcow2


/JJ