FROM ubuntu:latest

LABEL maintainer="joakim.l.johansson@ericsson.com"
LABEL references="Based on https://github.com/spujadas/lighttpd-docker"

RUN apt-get update \
  && apt-get install -y lighttpd supervisor libldap-2.4-2 \
  && rm -rf /var/lib/apt/lists/*

ADD alexserv_19_3_linux.tar.gz /usr/local
RUN chmod 755 /usr/local/ALEX/*

COPY ./configs/supervisord.conf ./configs/lighttpd.conf /etc/
COPY ./configs/alex.conf ./configs/omnidaemon.conf ./configs/sys_structure.conf /usr/local/ALEX/

VOLUME /data/alex/libraries
VOLUME /data/alex/writable

EXPOSE 80

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
